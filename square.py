from PIL import Image, ImageDraw, ImageFont
import unicodedata2 as unicodedata
from fontTools.ttLib import TTFont

total = 173746

n = 417 #   sqrt max element drawn. √173746

font_size = 17 #    Font drawn at 16 but fontforge adds a 1px border

#   Dimension of the image
width  = n*font_size
height = n*font_size

#   Image options
background_colour = "white"
font_colour       = "black"

#   Draw image
image = Image.new("1", (width, height), background_colour)
draw = ImageDraw.Draw(image)

# Then create a nested list with the length of the width of the square
nested_list= [[0 for i in range(n)] for j in range(n)]

#   Spiral code from https://github.com/ENGSamShamsan/spiral-shap-array
low=0
high=n-1

#   Number to be stored in matrix
x=(n*n)-1

levels=int((n+1)/2)
for level in range(levels):
    for i in range(low,high+1):
        nested_list[level][i]= x
        x-=1

    for i in range(low+1,high+1):
        nested_list[i][high]= x
        x-=1

    for i in range(high-1,low-1,-1):
        nested_list[high][i]= x
        x-=1

    for i in range(high-1,low,-1):
        nested_list[i][low]= x
        x-=1

    low+=1
    high-=1

#   Flip the lists so the top line reads left to right
for l in nested_list :
    l.reverse()

#   Iterate through the matrix
for i in range(n):
    for j in range(n):
        #   Get the number stored
        character_int = nested_list[i][j]
        #   Co-ordinates
        x = j * font_size
        y = i * font_size

        #   Draw initial control characters
        if character_int < 255 :
            character_png = Image.open('pngs/' + str(character_int) + ".png")
            image.paste( character_png, (round(x) , round(y)) )
        else:
            try:
                name = unicodedata.name(chr(character_int))
                character_png = Image.open('pngs/' + str(character_int) + ".png")
                image.paste( character_png, (x , y) )
            except:
                #   Character doesn't have a name - so leave a gap
                pass
                # print( "No character " + str(character_int) )

#   Draw title and copyleft
title_font_name = "unifont-14.0.04.ttf"
title_font = ImageFont.truetype(title_font_name, size=200)

draw.text(
    (1450, 6020),
    "Unicode 14 Squiral - ℅ @edent - 🄯 CC BY-SA",
    font=title_font,
    fill=font_colour)

image.save("Square.png")